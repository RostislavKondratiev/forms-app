import { Component } from '@angular/core';

@Component({
    selector: 'fa-sign-in',
    templateUrl: './signIn.component.html',
    styleUrls: ['./signIn.component.scss']
})
export class SignInComponent {}

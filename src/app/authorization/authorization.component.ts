import { Component } from '@angular/core';

@Component({
    selector: 'fa-authorization',
    templateUrl: './authorization.component.html',
    styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent {}

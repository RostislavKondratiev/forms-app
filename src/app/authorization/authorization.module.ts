import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/signIn.component';
import { SignUpComponent } from './sign-up/signUp.component';
import { AuthorizationComponent } from './authorization.component';
import { ROUTES } from './authorization.routes';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES)
    ],
    declarations: [
        SignInComponent,
        SignUpComponent,
        AuthorizationComponent
    ],
    providers: [],
    exports: []
})
export class AuthorizationModule {}
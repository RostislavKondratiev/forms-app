import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

const providers = [];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [

  ],
  providers: [],
  exports: [

  ]
})
export class UtilModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: UtilModule,
      providers: providers  // tslint:disable-line
    };
  }
}
